const gulp = require("gulp");
const ts = require("gulp-typescript");
const merge = require("merge2");

gulp.task("ts:dev", () => {
	return gulp.src(["src/**/*.ts"])
		.pipe(ts({
			module: "commonjs",
			target: "es6",
			noImplicitAny: true
		}))
		.pipe(gulp.dest("lib/"));
});

gulp.task("ts:dist", () => {
	const config = require("../tsconfig.json").compilerOptions;
	const result = gulp.src([config.rootDir + "/**/*.ts"])
		.pipe(ts(config));

	return merge([
		result.dts.pipe(gulp.dest("types/")),
		result.js.pipe(gulp.dest(config.outDir + "/"))
	]);
});
