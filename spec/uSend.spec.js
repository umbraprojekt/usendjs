const USend = require("../lib/index").USend;
const MailTransportError = require("../lib/index").MailTransportError;
const chai = require("chai");
chai.use(require("chai-as-promised"));
const expect = chai.expect;
const sinon = require("sinon");

describe("USend", () => {
	let usend;
	let templateEngine;
	let transport;

	beforeEach(() => {
		templateEngine = {
			isTemplateFile() {
				return false;
			},
			renderString(input) {
				return input;
			}
		};
		transport = {
			sendMail() {}
		};
		usend = new USend(templateEngine, transport);
	});

	it("should send a single email", () => {
		// given
		const messageConfig = {
			to: "abc",
			from: "def",
			subject: "ghi",
			text: "jkl"
		};
		const data = {};

		sinon.stub(transport, "sendMail").returns(Promise.resolve());

		// when
		const promise = usend.sendMail(messageConfig, data);

		// then
		expect(transport.sendMail.calledOnce).to.be.true;
		return expect(promise).to.be.fulfilled;
	});

	it("should reject when email can't be sent", () => {
		// given
		const messageConfig = {
			to: "abc",
			from: "def",
			subject: "ghi",
			text: "jkl"
		};
		const data = {};

		sinon.stub(transport, "sendMail").returns(Promise.reject(new Error()));

		// when
		const promise = usend.sendMail(messageConfig, data);

		// then
		expect(transport.sendMail.calledOnce).to.be.true;
		return expect(promise).to.be.rejectedWith(MailTransportError);
	});

	it("should batch send emails", () => {
		// given
		const messageConfigs = [{
			to: "abc",
			from: "def",
			subject: "ghi",
			text: "jkl"
		}, {
			to: "mno",
			from: "pqr",
			subject: "stu",
			text: "vwx"
		}];
		const data = {};

		sinon.stub(usend, "sendMail").returns(Promise.resolve());

		// when
		const promise = usend.sendMails(messageConfigs, data);

		// then
		expect(usend.sendMail.calledTwice).to.be.true;
		expect(usend.sendMail.firstCall.calledWithExactly(messageConfigs[0], data)).to.be.true;
		expect(usend.sendMail.secondCall.calledWithExactly(messageConfigs[1], data)).to.be.true;
		return expect(promise).to.be.fulfilled;
	});

	it("should reject when batch sending emails fails", () => {
		// given
		const messageConfigs = [{
			to: "abc",
			from: "def",
			subject: "ghi",
			text: "jkl"
		}, {
			to: "mno",
			from: "pqr",
			subject: "stu",
			text: "vwx"
		}];
		const data = {};

		sinon.stub(usend, "sendMail").returns(Promise.reject(new MailTransportError()));

		// when
		const promise = usend.sendMails(messageConfigs, data);

		// then
		return expect(promise).to.be.rejectedWith(MailTransportError);
	});

	it("should send email with default data", () => {
		// given
		const messageConfig = {
			to: "abc",
			from: "def",
			subject: "ghi",
			text: "jkl"
		};
		const data = {};
		const defaultData = {foo: "bar"};

		sinon.stub(transport, "sendMail").returns(Promise.resolve());
		sinon.stub(templateEngine, "renderString").returns("");

		// when
		usend.setDefaultData(defaultData);
		const promise = usend.sendMail(messageConfig, data);

		// then
		const outputData = templateEngine.renderString.getCall(0).args[1];
		expect(outputData).to.deep.equal(defaultData);
		return expect(promise).to.be.fulfilled;
	});

	it("should send email with default data overwritten by passed in data", () => {
		// given
		const messageConfig = {
			to: "abc",
			from: "def",
			subject: "ghi",
			text: "jkl"
		};
		const data = {foo: "qux"};
		const defaultData = {foo: "bar", bat: "man"};

		sinon.stub(transport, "sendMail").returns(Promise.resolve());
		sinon.stub(templateEngine, "renderString").returns("");

		// when
		usend.setDefaultData(defaultData);
		const promise = usend.sendMail(messageConfig, data);

		// then
		const outputData = templateEngine.renderString.getCall(0).args[1];
		expect(outputData).to.deep.equal({foo: "qux", bat: "man"});
		return expect(promise).to.be.fulfilled;
	});
});
