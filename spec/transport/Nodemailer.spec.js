const Nodemailer = require("../../lib/index").Nodemailer;
const chai = require("chai");
chai.use(require("chai-as-promised"));
const expect = chai.expect;
const sinon = require("sinon");

describe("Nodemailer", () => {
	let transporter;
	let nodemailer;

	beforeEach(() => {
		transporter = {
			sendMail() {}
		};
		nodemailer = new Nodemailer(transporter);
	});

	it("should use nodemailer to send email", () => {
		// given
		const message = {
			to: "abc",
			from: "def",
			subject: "ghi",
			text: "jkl"
		};

		const sendMailStub = sinon.stub(transporter, "sendMail");
		sendMailStub.withArgs(message).returns(Promise.resolve());

		// when
		const promise = nodemailer.sendMail(message);

		// then
		expect(transporter.sendMail.calledOnce).to.be.true;
		expect(transporter.sendMail.firstCall.calledWithExactly(message)).to.be.true;
		return expect(promise).to.be.fulfilled;
	});
});
