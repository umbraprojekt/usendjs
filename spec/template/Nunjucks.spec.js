const Nunjucks = require("../../lib/index").Nunjucks;
const chai = require("chai");
const expect = chai.expect;
const sinon = require("sinon");

describe("Nunjucks", () => {
	let nunjucks;
	let templateEngine;

	beforeEach(() => {
		templateEngine = {
			renderString() {},
			render() {}
		};
		nunjucks = new Nunjucks(templateEngine);
	});

	it("should determine which strings are template file names", () => {
		expect(nunjucks.isTemplateFile("readme.md")).to.be.false;
		expect(nunjucks.isTemplateFile("This is not a file name")).to.be.false;
		expect(nunjucks.isTemplateFile("templates/template.html")).to.be.true;
		expect(nunjucks.isTemplateFile("templates/template.twig")).to.be.true;
	});

	it("should enable setting template extension", () => {
		// when
		nunjucks.setTemplateExtensions("nunj");

		// then
		expect(nunjucks.isTemplateFile("templates/template.html")).to.be.false;
		expect(nunjucks.isTemplateFile("templates/template.twig")).to.be.false;
		expect(nunjucks.isTemplateFile("templates/template.nunj")).to.be.true;
	});

	it("should render a string", () => {
		// given
		const template = "This is {{place}}!";
		const data = {place: "Sparta"};
		const expectedResult = "This is Sparta!";

		const renderStringStub = sinon.stub(templateEngine, "renderString");
		renderStringStub.withArgs(template, data).returns(expectedResult);

		// when
		const result = nunjucks.renderString(template, data);

		// then
		expect(result).to.equal(expectedResult);
	});

	it("should render a file", () => {
		// given
		const template = "template/sparta.html";
		const data = {place: "Sparta"};
		const expectedResult = "This is Sparta!";

		const renderFileStub = sinon.stub(templateEngine, "render");
		renderFileStub.withArgs(template, data).returns(expectedResult);

		// when
		const result = nunjucks.renderFile(template, data);

		// then
		expect(result).to.equal(expectedResult);
	});
});
