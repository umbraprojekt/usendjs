const MessageFactory = require("../../lib/index").MessageFactory;
const chai = require("chai");
chai.use(require("chai-as-promised"));
const expect = chai.expect;
const sinon = require("sinon");

describe("MessageFactory", () => {
	let messageFactory;
	let templateEngine;

	beforeEach(() => {
		templateEngine = {
			isTemplateFile() {},
			renderString() {},
			renderFile() {}
		};
		messageFactory = new MessageFactory(templateEngine);
	});

	it("should build a message using plain strings", () => {
		// given
		const messageConfiguration = {
			from: "foo@example.com",
			to: "bar@example.com",
			subject: "Foo Bar",
			text: "This is a Foo Bar message!"
		};
		const data = {};

		sinon.stub(templateEngine, "isTemplateFile").returns(false);
		sinon.stub(templateEngine, "renderString").returnsArg(0);

		// when
		const message = messageFactory.build(messageConfiguration, data);

		// then
		expect(message.from).to.equal(messageConfiguration.from);
		expect(message.to).to.equal(messageConfiguration.to);
		expect(message.subject).to.equal(messageConfiguration.subject);
		expect(message.text).to.equal(messageConfiguration.text);
	});

	it("should build a message using template strings", () => {
		// given
		const messageConfiguration = {
			from: "{{from}}",
			to: "{{to}}",
			subject: "{{subject}}",
			text: "{{message}}"
		};
		const data = {
			from: "foo@example.com",
			to: "bar@example.com",
			subject: "Foo Bar",
			message: "This is a Foo Bar message!"
		};

		sinon.stub(templateEngine, "isTemplateFile").returns(false);
		sinon.stub(templateEngine, "renderString").callsFake((input, data) => {
			switch(input) {
				case messageConfiguration.from: return data.from;
				case messageConfiguration.to: return data.to;
				case messageConfiguration.subject: return data.subject;
				case messageConfiguration.text: return data.message;
				default: throw new Error("Invalid input");
			}
		});

		// when
		const message = messageFactory.build(messageConfiguration, data);

		// then
		expect(message.from).to.equal(data.from);
		expect(message.to).to.equal(data.to);
		expect(message.subject).to.equal(data.subject);
		expect(message.text).to.equal(data.message);
	});

	it("should build a message using email address configuration objects", () => {
		// given
		const messageConfiguration = {
			from: {name: "Foo", email: "foo@example.com"},
			to: "bar@example.com",
			subject: "Foo Bar",
			text: "This is a Foo Bar message!"
		};
		const data = {};

		sinon.stub(templateEngine, "isTemplateFile").returns(false);
		sinon.stub(templateEngine, "renderString").returnsArg(0);

		// when
		const message = messageFactory.build(messageConfiguration, data);

		// then
		expect(message.from).to.equal("\"Foo\" <foo@example.com>");
		expect(message.to).to.equal(messageConfiguration.to);
		expect(message.subject).to.equal(messageConfiguration.subject);
		expect(message.text).to.equal(messageConfiguration.text);
	});

	it("should build a message using an array of strings", () => {
		// given
		const messageConfiguration = {
			from: ["foo@example.com", "qux@example.com"],
			to: "bar@example.com",
			subject: "Foo Bar",
			text: "This is a Foo Bar message!"
		};
		const data = {};

		sinon.stub(templateEngine, "isTemplateFile").returns(false);
		sinon.stub(templateEngine, "renderString").returnsArg(0);

		// when
		const message = messageFactory.build(messageConfiguration, data);

		// then
		expect(message.from).to.equal("foo@example.com, qux@example.com");
		expect(message.to).to.equal(messageConfiguration.to);
		expect(message.subject).to.equal(messageConfiguration.subject);
		expect(message.text).to.equal(messageConfiguration.text);
	});

	it("should build a message using an array of email address configuration objects", () => {
		// given
		const messageConfiguration = {
			from: [{name: "Foo", email: "foo@example.com"}, {name: "Qux", email: "qux@example.com"}],
			to: "bar@example.com",
			subject: "Foo Bar",
			text: "This is a Foo Bar message!"
		};
		const data = {};

		sinon.stub(templateEngine, "isTemplateFile").returns(false);
		sinon.stub(templateEngine, "renderString").returnsArg(0);

		// when
		const message = messageFactory.build(messageConfiguration, data);

		// then
		expect(message.from).to.equal("\"Foo\" <foo@example.com>, \"Qux\" <qux@example.com>");
		expect(message.to).to.equal(messageConfiguration.to);
		expect(message.subject).to.equal(messageConfiguration.subject);
		expect(message.text).to.equal(messageConfiguration.text);
	});

	it("should build a message using a template file", () => {
		// given
		const messageConfiguration = {
			from: "foo@example.com",
			to: "bar@example.com",
			subject: "Foo Bar",
			text: "template.html"
		};
		const data = {};
		const fileContent = "This is a file content!";

		sinon.stub(templateEngine, "isTemplateFile").callsFake(input => input === messageConfiguration.text);
		sinon.stub(templateEngine, "renderString").returnsArg(0);
		const renderFileStub = sinon.stub(templateEngine, "renderFile");
		renderFileStub.withArgs(messageConfiguration.text, data).returns(fileContent);

		// when
		const message = messageFactory.build(messageConfiguration, data);

		// then
		expect(message.from).to.equal(messageConfiguration.from);
		expect(message.to).to.equal(messageConfiguration.to);
		expect(message.subject).to.equal(messageConfiguration.subject);
		expect(message.text).to.equal(fileContent);
	});

	it("should include optional fields", () => {
		// given
		const messageConfiguration = {
			from: "from@example.com",
			to: "to@example.com",
			cc: "cc@example.com",
			bcc: "bcc@example.com",
			replyTo: "replyTo@example.com",
			subject: "Foo Bar",
			text: "This is a Foo Bar message!"
		};
		const data = {};

		sinon.stub(templateEngine, "isTemplateFile").returns(false);
		sinon.stub(templateEngine, "renderString").returnsArg(0);

		// when
		const message = messageFactory.build(messageConfiguration, data);

		// then
		expect(message.cc).to.equal(messageConfiguration.cc);
		expect(message.bcc).to.equal(messageConfiguration.bcc);
		expect(message.replyTo).to.equal(messageConfiguration.replyTo);
	});
});
