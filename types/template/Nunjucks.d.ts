import { TemplateEngine } from "./TemplateEngine";
export declare class Nunjucks implements TemplateEngine {
    private _nunjucks;
    private _extensions;
    constructor(nunjucks: any);
    renderString(template: string, data: any): string;
    renderFile(file: string, data: any): string;
    setTemplateExtensions(extensions: string | Array<string>): void;
    isTemplateFile(path: string): boolean;
}
