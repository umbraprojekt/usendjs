export declare class MailTransportError extends Error {
    constructor();
    constructor(message: string);
    constructor(error: Error);
    constructor(message?: string, error?: Error);
}
