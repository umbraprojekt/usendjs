import { MessageConfiguration } from "./message/MessageConfiguration";
import { TemplateEngine } from "./template/TemplateEngine";
import { Transport } from "./transport/Transport";
export declare class USend {
    private _templateEngine;
    private _transport;
    private _data;
    constructor(templateEngine: TemplateEngine, transport: Transport);
    sendMail(messageConfig: MessageConfiguration, data: any): Promise<void>;
    sendMails(messageConfigs: Array<MessageConfiguration>, data: any): Promise<void[]>;
    setDefaultData(data: any): void;
}
