import { Transport } from "./Transport";
import { Message } from "../message/Message";
export declare class Nodemailer implements Transport {
    private _transporter;
    constructor(transporter: any);
    sendMail(message: Message): Promise<void>;
}
