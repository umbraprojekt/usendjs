import { Message } from "../message/Message";
export interface Transport {
    sendMail(message: Message): Promise<void>;
}
