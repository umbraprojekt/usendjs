import { MessageConfiguration } from "./MessageConfiguration";
import { Message } from "./Message";
import { TemplateEngine } from "../template/TemplateEngine";
export declare class MessageFactory {
    private _templateEngine;
    constructor(templateEngine: TemplateEngine);
    build(config: MessageConfiguration, data: any): Message;
    private _transformAddress;
    private _transformTemplateString;
}
