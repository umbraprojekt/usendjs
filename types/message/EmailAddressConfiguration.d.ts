export interface EmailAddressConfiguration {
    name: string;
    email: string;
}
