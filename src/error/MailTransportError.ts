export class MailTransportError extends Error {
	constructor();
	constructor(message: string);
	constructor(error: Error);
	constructor(message?: string, error?: Error);
	constructor(message?: any, error?: any) {
		const defaultMessage = "An error occurred when attempting to send email message.";
		switch (arguments.length) {
			case 0:
				super(defaultMessage);
				break;
			case 1:
				if (typeof message === "string") {
					super(message);
				} else {
					super(message.message);
					this.stack += "\nCaused by: " + message.stack;
				}
				break;
			case 2:
				super(message || defaultMessage);
				this.stack += "\nCaused by: " + error.stack;
				break;
		}
	}
}
