import {MessageConfiguration} from "./message/MessageConfiguration";
import {TemplateEngine} from "./template/TemplateEngine";
import {MessageFactory} from "./message/MessageFactory";
import {Message} from "./message/Message";
import {Transport} from "./transport/Transport";
import {MailTransportError} from "./error/MailTransportError";

export class USend {
	private _templateEngine: TemplateEngine;
	private _transport: Transport;
	private _data: any = {};

	public constructor(templateEngine: TemplateEngine, transport: Transport) {
		this._templateEngine = templateEngine;
		this._transport = transport;
	}

	public sendMail(messageConfig: MessageConfiguration, data: any): Promise<void> {
		const input: any = {};
		Object.keys(this._data).forEach(key => input[key] = this._data[key]);
		Object.keys(data).forEach(key => input[key] = data[key]);

		const message: Message = new MessageFactory(this._templateEngine)
			.build(messageConfig, input);
		return this._transport.sendMail(message)
			.catch(e => {
				throw new MailTransportError(e);
			});
	}

	public sendMails(messageConfigs: Array<MessageConfiguration>, data: any): Promise<void[]> {
		return Promise.all(messageConfigs.map(messageConfig => this.sendMail(messageConfig, data)));
	}

	public setDefaultData(data: any): void {
		this._data = data;
	}
}
