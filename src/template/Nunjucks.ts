import {TemplateEngine} from "./TemplateEngine";

export class Nunjucks implements TemplateEngine {
	private _nunjucks: any;
	private _extensions: Array<string>;

	public constructor(nunjucks: any) {
		this._nunjucks = nunjucks;
		this._extensions = ["html", "twig", "njk", ".nunjucks", ".nunjs", ".nj", ".htm", ".template", ".tmpl", ".tpl"];
	}

	public renderString(template: string, data: any): string {
		return this._nunjucks.renderString(template, data);
	}

	public renderFile(file: string, data: any): string {
		return this._nunjucks.render(file, data);
	}

	public setTemplateExtensions(extensions: string|Array<string>): void {
		if (typeof extensions === "string") {
			extensions = [extensions];
		}

		this._extensions = extensions;
	}

	public isTemplateFile(path: string): boolean {
		const regex = new RegExp(`.+\.(${this._extensions.join("|")})$`);
		return regex.test(path);

	}
}
