export interface TemplateEngine {
	renderString(template: string, data: any): string;
	renderFile(file: string, data: any): string;
	isTemplateFile(path: string): boolean;
	setTemplateExtensions(extensions: string|Array<string>): void;
}
