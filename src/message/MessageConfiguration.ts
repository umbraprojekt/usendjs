import {EmailAddressConfiguration} from "./EmailAddressConfiguration";

export interface MessageConfiguration {
	text?: string;
	html?: string;
	subject: string;
	from: string|EmailAddressConfiguration;
	replyTo?: string|EmailAddressConfiguration;
	to: string|EmailAddressConfiguration|Array<string|EmailAddressConfiguration>;
	cc?: string|EmailAddressConfiguration|Array<string|EmailAddressConfiguration>;
	bcc?: string|EmailAddressConfiguration|Array<string|EmailAddressConfiguration>;
}
