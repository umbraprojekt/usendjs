export interface Message {
	text?: string;
	html?: string;
	subject: string;
	from: string;
	replyTo?: string;
	to: string;
	cc?: string;
	bcc?: string;
}
