import {Transport} from "./Transport";
import {Message} from "../message/Message";

export class Nodemailer implements Transport {
	private _transporter: any;

	constructor(transporter: any) {
		this._transporter = transporter;
	}

	public sendMail(message: Message): Promise<void> {
		return this._transporter.sendMail(message);
	}
}
