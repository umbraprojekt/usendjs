console.log("USend joins together the functionality of an email transporter and a templating engine, but depends on neither.");
console.log("In order to use uSend, you need to install both an email transporter and a templating engine dependency.");
console.log("Recommended packages are nodemailer and nunjucks. In order to install them, please run:\n");
console.log("    npm install nodemailer nunjucks --save\n");
