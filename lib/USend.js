"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MessageFactory_1 = require("./message/MessageFactory");
const MailTransportError_1 = require("./error/MailTransportError");
class USend {
    constructor(templateEngine, transport) {
        this._data = {};
        this._templateEngine = templateEngine;
        this._transport = transport;
    }
    sendMail(messageConfig, data) {
        const input = {};
        Object.keys(this._data).forEach(key => input[key] = this._data[key]);
        Object.keys(data).forEach(key => input[key] = data[key]);
        const message = new MessageFactory_1.MessageFactory(this._templateEngine)
            .build(messageConfig, input);
        return this._transport.sendMail(message)
            .catch(e => {
            throw new MailTransportError_1.MailTransportError(e);
        });
    }
    sendMails(messageConfigs, data) {
        return Promise.all(messageConfigs.map(messageConfig => this.sendMail(messageConfig, data)));
    }
    setDefaultData(data) {
        this._data = data;
    }
}
exports.USend = USend;
