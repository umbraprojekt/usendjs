"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Nodemailer {
    constructor(transporter) {
        this._transporter = transporter;
    }
    sendMail(message) {
        return this._transporter.sendMail(message);
    }
}
exports.Nodemailer = Nodemailer;
