"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MessageFactory {
    constructor(templateEngine) {
        this._templateEngine = templateEngine;
    }
    build(config, data) {
        const message = {
            to: this._transformAddress(config.to, data),
            from: this._transformAddress(config.from, data),
            subject: this._transformTemplateString(config.subject, data)
        };
        if (config.replyTo) {
            message.replyTo = this._transformAddress(config.replyTo, data);
        }
        if (config.cc) {
            message.cc = this._transformAddress(config.cc, data);
        }
        if (config.bcc) {
            message.bcc = this._transformAddress(config.bcc, data);
        }
        if (config.text) {
            message.text = this._transformTemplateString(config.text, data);
        }
        if (config.html) {
            message.html = this._transformTemplateString(config.html, data);
        }
        return message;
    }
    _transformAddress(config, data) {
        const addresses = [];
        if (Array.isArray(config)) {
            config.forEach(address => addresses.push(this._transformAddress(address, data)));
        }
        else if (typeof config === "string") {
            addresses.push(this._transformTemplateString(config, data));
        }
        else {
            addresses.push(`"${this._transformTemplateString(config.name, data)}" ` +
                `<${this._transformTemplateString(config.email, data)}>`);
        }
        return addresses.join(", ");
    }
    _transformTemplateString(template, data) {
        if (this._templateEngine.isTemplateFile(template)) {
            return this._templateEngine.renderFile(template, data);
        }
        return this._templateEngine.renderString(template, data);
    }
}
exports.MessageFactory = MessageFactory;
