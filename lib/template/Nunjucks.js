"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Nunjucks {
    constructor(nunjucks) {
        this._nunjucks = nunjucks;
        this._extensions = ["html", "twig", "njk", ".nunjucks", ".nunjs", ".nj", ".htm", ".template", ".tmpl", ".tpl"];
    }
    renderString(template, data) {
        return this._nunjucks.renderString(template, data);
    }
    renderFile(file, data) {
        return this._nunjucks.render(file, data);
    }
    setTemplateExtensions(extensions) {
        if (typeof extensions === "string") {
            extensions = [extensions];
        }
        this._extensions = extensions;
    }
    isTemplateFile(path) {
        const regex = new RegExp(`.+\.(${this._extensions.join("|")})$`);
        return regex.test(path);
    }
}
exports.Nunjucks = Nunjucks;
